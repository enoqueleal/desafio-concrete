package com.concrete.service;

import com.concrete.model.PullRequest;
import com.concrete.dto.RepositoryDTO;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by enoque.felipe.d.leal on 30/09/2017.
 */

public interface RepositoryService {

    @GET("search/repositories")
    Call<RepositoryDTO> getGitRepositories(@Query("q") String language, @Query("sort") String sort, @Query("page") String page);

    @GET("repos/{criador}/{repositorio}/pulls")
    Call<PullRequest[]> getGitPullRequests(@Path(value = "criador") String criador, @Path(value = "repositorio") String repositorio);

}
