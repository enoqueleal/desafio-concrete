package com.concrete.dto;

import com.concrete.model.Repository;

import java.util.List;

/**
 * Created by enoque.felipe on 30/09/2017.
 */

public class RepositoryDTO {

    private String total_count;
    private Boolean incomplete_results;
    private List<Repository> items;

    public String getTotal_count() {
        return total_count;
    }

    public void setTotal_count(String total_count) {
        this.total_count = total_count;
    }

    public Boolean getIncomplete_results() {
        return incomplete_results;
    }

    public void setIncomplete_results(Boolean incomplete_results) {
        this.incomplete_results = incomplete_results;
    }

    public List<Repository> getItems() {
        return items;
    }

    public void setItems(List<Repository> items) {
        this.items = items;
    }
    
}
