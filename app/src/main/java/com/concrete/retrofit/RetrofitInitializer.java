package com.concrete.retrofit;

import com.concrete.model.Repository;
import com.concrete.service.RepositoryService;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by enoque.felipe on 30/09/2017.
 */

public class RetrofitInitializer {

    private final Retrofit retrofit;

    public RetrofitInitializer() {
        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }

    public RepositoryService getRepositoriesService() {
        return retrofit.create(RepositoryService.class);
    }

}
