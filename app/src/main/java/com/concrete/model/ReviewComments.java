package com.concrete.model;

import java.io.Serializable;

public class ReviewComments implements Serializable {

    private String href;

    /**
     * No args constructor for use in serialization
     */
    public ReviewComments() {
    }

    /**
     * @param href
     */
    public ReviewComments(String href) {
        super();
        this.href = href;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

}
