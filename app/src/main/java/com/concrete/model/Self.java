package com.concrete.model;

import java.io.Serializable;

public class Self implements Serializable {

    private String href;

    /**
     * No args constructor for use in serialization
     */
    public Self() {
    }

    /**
     * @param href
     */
    public Self(String href) {
        super();
        this.href = href;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

}
