package com.concrete.model;

import java.io.Serializable;

public class Base implements Serializable {

    private String label;
    private String ref;
    private String sha;
    private User user;
    private Repo repo;

    /**
     * No args constructor for use in serialization
     */
    public Base() {
    }

    /**
     * @param ref
     * @param sha
     * @param repo
     * @param label
     * @param user
     */
    public Base(String label, String ref, String sha, User user, Repo repo) {
        super();
        this.label = label;
        this.ref = ref;
        this.sha = sha;
        this.user = user;
        this.repo = repo;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Repo getRepo() {
        return repo;
    }

    public void setRepo(Repo repo) {
        this.repo = repo;
    }

}
