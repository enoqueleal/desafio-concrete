package com.concrete.model;

import java.io.Serializable;

public class Links implements Serializable {

    private Self self;
    private Html html;
    private Issue issue;
    private Comments comments;
    private ReviewComments reviewComments;
    private ReviewComment reviewComment;
    private Commits commits;
    private Statuses statuses;

    /**
     * No args constructor for use in serialization
     */
    public Links() {
    }

    /**
     * @param issue
     * @param commits
     * @param html
     * @param self
     * @param reviewComments
     * @param statuses
     * @param reviewComment
     * @param comments
     */
    public Links(Self self, Html html, Issue issue, Comments comments, ReviewComments reviewComments, ReviewComment reviewComment, Commits commits, Statuses statuses) {
        super();
        this.self = self;
        this.html = html;
        this.issue = issue;
        this.comments = comments;
        this.reviewComments = reviewComments;
        this.reviewComment = reviewComment;
        this.commits = commits;
        this.statuses = statuses;
    }

    public Self getSelf() {
        return self;
    }

    public void setSelf(Self self) {
        this.self = self;
    }

    public Html getHtml() {
        return html;
    }

    public void setHtml(Html html) {
        this.html = html;
    }

    public Issue getIssue() {
        return issue;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    public ReviewComments getReviewComments() {
        return reviewComments;
    }

    public void setReviewComments(ReviewComments reviewComments) {
        this.reviewComments = reviewComments;
    }

    public ReviewComment getReviewComment() {
        return reviewComment;
    }

    public void setReviewComment(ReviewComment reviewComment) {
        this.reviewComment = reviewComment;
    }

    public Commits getCommits() {
        return commits;
    }

    public void setCommits(Commits commits) {
        this.commits = commits;
    }

    public Statuses getStatuses() {
        return statuses;
    }

    public void setStatuses(Statuses statuses) {
        this.statuses = statuses;
    }

}
