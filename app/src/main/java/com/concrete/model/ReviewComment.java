package com.concrete.model;

import java.io.Serializable;

public class ReviewComment implements Serializable {

    private String href;

    /**
     * No args constructor for use in serialization
     */
    public ReviewComment() {
    }

    /**
     * @param href
     */
    public ReviewComment(String href) {
        super();
        this.href = href;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

}
