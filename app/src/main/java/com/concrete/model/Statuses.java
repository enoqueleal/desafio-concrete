package com.concrete.model;

import java.io.Serializable;

public class Statuses implements Serializable {

    private String href;

    /**
     * No args constructor for use in serialization
     */
    public Statuses() {
    }

    /**
     * @param href
     */
    public Statuses(String href) {
        super();
        this.href = href;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

}
