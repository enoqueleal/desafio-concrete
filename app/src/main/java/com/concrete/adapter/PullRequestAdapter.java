package com.concrete.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.concrete.R;
import com.concrete.model.PullRequest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrador on 06/10/2017.
 */

public class PullRequestAdapter extends BaseAdapter {

    private final List<PullRequest> items;
    private final Context context;

    public PullRequestAdapter(Context context, List<PullRequest> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = convertView;

        if (view == null) {
            view = inflater.inflate(R.layout.list_prs, parent, false);
        }

        ImageView image = (ImageView) view.findViewById(R.id.user_img);
        TextView title = (TextView) view.findViewById(R.id.prs_title);
        TextView description = (TextView) view.findViewById(R.id.prs_description);
        TextView nickname = (TextView) view.findViewById(R.id.user_nickname);
        TextView fullName = (TextView) view.findViewById(R.id.user_full_name);

        String imgUri = items.get(position).getUser().getAvatar_url();

        if (null != imgUri || "" != imgUri) {
            Picasso.with(context).load(imgUri).into(image);
        }

        title.setText(items.get(position).getTitle());
        description.setText(items.get(position).getBody());
        nickname.setText(items.get(position).getUser().getLogin());
        fullName.setText(items.get(position).getUser().getName());

        return view;
    }
}
