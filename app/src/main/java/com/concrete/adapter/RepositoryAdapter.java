package com.concrete.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.concrete.R;
import com.concrete.model.Repository;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by enoque.felipe on 30/09/2017.
 */

public class RepositoryAdapter extends BaseAdapter {

    private final List<Repository> items;
    private final Context context;

    public RepositoryAdapter(Context context, List<Repository> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = convertView;

        if (view == null) {
            view = inflater.inflate(R.layout.list_repositories, parent, false);
        }

        ImageView image = (ImageView) view.findViewById(R.id.user_img);
        TextView nameRepository = (TextView) view.findViewById(R.id.repository_name);
        TextView description = (TextView) view.findViewById(R.id.repository_description);
        TextView userNickname = (TextView) view.findViewById(R.id.repository_nickname);
        TextView fullName = (TextView) view.findViewById(R.id.repository_username);
        TextView forks = (TextView) view.findViewById(R.id.repository_forks);
        TextView stars = (TextView) view.findViewById(R.id.repository_star);

        nameRepository.setText(items.get(position).getName());
        description.setText(items.get(position).getDescription());
        userNickname.setText(items.get(position).getOwner().getLogin());
        fullName.setText(items.get(position).getFull_name());
        forks.setText(items.get(position).getForks_count().toString());
        stars.setText(items.get(position).getStargazers_count().toString());

        String imgUri = items.get(position).getOwner().getAvatar_url();

        if (null != imgUri || "" != imgUri) {
            Picasso.with(context).load(imgUri).into(image);
        }

        return view;
    }

}
