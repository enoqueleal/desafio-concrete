package com.concrete.activity;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.concrete.R;
import com.concrete.adapter.PullRequestAdapter;
import com.concrete.model.PullRequest;
import com.concrete.model.Repository;
import com.concrete.retrofit.RetrofitInitializer;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListPullRequestsActivity extends AppCompatActivity {

    private Repository repository;
    private ListView listPullRequests;
    private PullRequest[] pulls;
    private int quantityOpened = 0;
    private int quantityClosed = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pull_requests);

        listPullRequests = (ListView) findViewById(R.id.list_pull_requests);

        repository = (Repository) getIntent().getSerializableExtra("repository");

        getPullRequets();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private PullRequest[] getPullRequets() {

        Call<PullRequest[]> call = new RetrofitInitializer().getRepositoriesService().getGitPullRequests(repository.getOwner().getLogin(), repository.getName());

        call.enqueue(new Callback<PullRequest[]>() {
            @Override
            public void onResponse(Call<PullRequest[]> call, Response<PullRequest[]> response) {

                pulls = response.body();
                mountData(pulls);

                System.out.println(call.request().url());
                System.out.println("Código: " + response.code());
            }

            @Override
            public void onFailure(Call<PullRequest[]> call, Throwable t) {
                System.out.println(call.request().url());
                Log.e("onFailure: ", t.getMessage());
            }
        });

        ListPullRequestsActivity.this.setTitle(repository.getName());

        return pulls;
    }

    private void mountData(PullRequest[] dados) {

        List<PullRequest> items;

        if (0 == dados.length) {
            items = new ArrayList<>();
        } else {
            items = Arrays.asList(dados);
        }

        for (PullRequest prs : items) {
            if (prs.getState().equals("open")) {
                quantityOpened++;
            } else if (prs.getState().equals("closed")) {
                quantityClosed++;
            }
        }

        TextView opened = (TextView) findViewById(R.id.quantity_opened);
        TextView closed = (TextView) findViewById(R.id.quantity_closed);
        TextView separator = (TextView) findViewById(R.id.separator);

        opened.setText(quantityOpened + " opened");
        separator.setText(" / ");
        closed.setText(quantityClosed + " closed");

        PullRequestAdapter adapter = new PullRequestAdapter(this, items);
        listPullRequests.setAdapter(adapter);

    }
}
