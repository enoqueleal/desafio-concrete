package com.concrete.activity;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.concrete.R;
import com.concrete.adapter.RepositoryAdapter;
import com.concrete.dto.RepositoryDTO;
import com.concrete.model.Repository;
import com.concrete.retrofit.RetrofitInitializer;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListRepositoriesActivity extends AppCompatActivity {

    private RepositoryDTO repositoryDTO;
    private ListView listRepositories;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_repositories);

        listRepositories = (ListView) findViewById(R.id.list_repositories);

        startAsyncCall();

        listRepositories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View item, int position, long id) {

                Repository repository = (Repository) listRepositories.getItemAtPosition(position);

                Intent intent = new Intent(ListRepositoriesActivity.this, ListPullRequestsActivity.class);

                intent.putExtra("repository", repository);

                startActivity(intent);
            }
        });

    }

    private void startAsyncCall() {
        Call<RepositoryDTO> call = new RetrofitInitializer().getRepositoriesService().getGitRepositories("language:Java", "stars", "1");
        spinner = (ProgressBar) findViewById(R.id.progress_bar_repository);
        spinner.setVisibility(View.VISIBLE);

        call.enqueue(new Callback<RepositoryDTO>() {
            @Override
            public void onResponse(Call<RepositoryDTO> call, Response<RepositoryDTO> response) {

                if (response.code() == 200) {
                    repositoryDTO = response.body();
                    mountData(repositoryDTO.getItems());
                    spinner.setVisibility(View.INVISIBLE);
                } else {

                }

            }

            @Override
            public void onFailure(Call<RepositoryDTO> call, Throwable t) {
                Log.e("onFailure: ", t.getMessage());
            }

        });
    }

    private void mountData(List<Repository> items) {
        RepositoryAdapter adapter = new RepositoryAdapter(this, items);
        listRepositories.setAdapter(adapter);
    }

}
